package com.gustavodamaral.clubsupporter.exception;

import org.springframework.http.HttpStatus;

public class RestCommunicationException extends RuntimeException {

    private final Object body;
    private final HttpStatus httpStatus;

    public RestCommunicationException(String body, HttpStatus httpStatus) {
        this.body = body;
        this.httpStatus = httpStatus;
    }
}