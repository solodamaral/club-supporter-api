package com.gustavodamaral.clubsupporter.configuration;

import com.gustavodamaral.clubsupporter.communication.config.RestServiceConfigurationCampaignsImpl;
import com.gustavodamaral.clubsupporter.communication.strategy.RestTemplateStrategy;
import com.gustavodamaral.clubsupporter.communication.strategy.RestTemplateStrategyImpl;
import com.gustavodamaral.clubsupporter.communication.url.RestTemplateStrategyBuilderImpl;
import com.gustavodamaral.clubsupporter.communication.url.UrlBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestServiceConfig {

    @Bean("restTemplateCampaign")
    public RestTemplateStrategy restTemplateCampaign(RestServiceConfigurationCampaignsImpl configuration) {
        return new RestTemplateStrategyImpl(new RestTemplateStrategyBuilderImpl(new UrlBuilder(configuration)));
    }

}