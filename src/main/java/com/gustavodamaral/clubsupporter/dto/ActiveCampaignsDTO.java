package com.gustavodamaral.clubsupporter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor @NoArgsConstructor
public class ActiveCampaignsDTO {
    private List<CampaignResponseDTO> activeCampaigns;
}
