package com.gustavodamaral.clubsupporter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@Builder
@AllArgsConstructor @NoArgsConstructor
public class CampaignResponseDTO {
    private Long id;
    private String name;
    private Long teamId;
    private LocalDate effectiveDateStart;
    private LocalDate effectiveDateEnd;
}
