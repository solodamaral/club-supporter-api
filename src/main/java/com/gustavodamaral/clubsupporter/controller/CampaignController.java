package com.gustavodamaral.clubsupporter.controller;

import com.gustavodamaral.clubsupporter.dto.ActiveCampaignsDTO;
import com.gustavodamaral.clubsupporter.dto.CampaignResponseDTO;
import com.gustavodamaral.clubsupporter.service.CampaignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Api(value = "Campaign")
@RestController
@RequestMapping(value = "/campaign")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @ApiOperation(nickname = "getActiveCampaigns", value = "")
    @GetMapping(value = "")
    public ResponseEntity<ActiveCampaignsDTO> getActiveCampaigns() {
        return campaignService.test();
    }

}
