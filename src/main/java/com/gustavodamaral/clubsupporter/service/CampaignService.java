package com.gustavodamaral.clubsupporter.service;

import com.gustavodamaral.clubsupporter.communication.strategy.Request;
import com.gustavodamaral.clubsupporter.communication.strategy.RestTemplateStrategy;
import com.gustavodamaral.clubsupporter.dto.ActiveCampaignsDTO;
import com.gustavodamaral.clubsupporter.dto.CampaignResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampaignService {

    @Autowired
    @Qualifier("restTemplateCampaign")
    private RestTemplateStrategy restTemplateUtils;

    public ResponseEntity<ActiveCampaignsDTO> test() {

        Request request = Request.builder().url("/campaign").build();
        ResponseEntity<ActiveCampaignsDTO> teste = restTemplateUtils.doGet(request, ActiveCampaignsDTO.class);
        return teste;
    }

}
