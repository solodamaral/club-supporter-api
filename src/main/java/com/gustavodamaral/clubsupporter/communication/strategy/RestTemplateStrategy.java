package com.gustavodamaral.clubsupporter.communication.strategy;

import org.springframework.http.ResponseEntity;

public interface RestTemplateStrategy {

    <T> ResponseEntity doGet(Request request, Class<T> responseBody);

    <T> ResponseEntity doDelete(Request request, Class<T> responseBody);

    <R, T> ResponseEntity doPost(Request request, R requestBody, Class<T> responseBody);

    <R, T> ResponseEntity doPut(Request request, R requestBody, Class<T> responseBody);

}