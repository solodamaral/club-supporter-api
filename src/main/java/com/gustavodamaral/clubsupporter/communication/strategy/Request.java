package com.gustavodamaral.clubsupporter.communication.strategy;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;


@Getter
@Setter
public class Request{
    String url;
    HttpMethod httpMethod;
    
    @Builder
    public Request(String url, HttpMethod httpMethod) {
    	this.url = url;
    	this.httpMethod = httpMethod;
    }
}