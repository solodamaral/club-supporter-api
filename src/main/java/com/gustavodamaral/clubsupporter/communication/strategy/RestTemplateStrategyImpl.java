package com.gustavodamaral.clubsupporter.communication.strategy;

import com.gustavodamaral.clubsupporter.communication.url.RestTemplateStrategyBuilder;
import com.gustavodamaral.clubsupporter.exception.RestCommunicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;

@Component
@Slf4j
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RestTemplateStrategyImpl implements RestTemplateStrategy {

    private RestTemplateStrategyBuilder restTemplateBuilder;

    public RestTemplateStrategyImpl(RestTemplateStrategyBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    public <T> ResponseEntity doGet(Request request, Class<T> responseBody) {
        request.setHttpMethod(HttpMethod.GET);
        return doExchange(request, doHttpEntity(request), responseBody);
    }

    public <T> ResponseEntity doDelete(Request request, Class<T> responseBody) {
        request.setHttpMethod(HttpMethod.DELETE);
        return doExchange(request, doHttpEntity(request), responseBody);
    }

    public <R, T> ResponseEntity doPost(Request request, R requestBody, Class<T> responseBody) {
        request.setHttpMethod(HttpMethod.POST);
        return doExchange(request, doHttpEntity(request, requestBody), responseBody);
    }

    public <R, T> ResponseEntity doPut(Request request, R requestBody, Class<T> responseBody) {
        request.setHttpMethod(HttpMethod.PUT);
        return doExchange(request, doHttpEntity(request, requestBody), responseBody);
    }

    private <R, T> ResponseEntity<T> doExchange(Request request, HttpEntity<R> httpEntity, Class<T> responseBody) {
        restTemplateBuilder.getUrlBuilder().fetchUrl(request);

        ResponseEntity<T> responseEntity = null;
        try {
            responseEntity = restTemplateBuilder.build().exchange(request.getUrl(), request.getHttpMethod(), httpEntity, responseBody);
        } catch (HttpStatusCodeException e) {
            doUnexpectedResponseError(e);
        } catch (ResourceAccessException e) {
            doResourceAccessExceptionError(e);
        } catch (Exception e) {
        }
        return responseEntity;
    }

    private void doResourceAccessExceptionError(ResourceAccessException e) {
        throw new RestCommunicationException(null, HttpStatus.SERVICE_UNAVAILABLE);
    }

    private void doUnexpectedResponseError(HttpStatusCodeException e) {
        throw new RestCommunicationException(e.getResponseBodyAsString(), e.getStatusCode());
    }

    private HttpEntity doHttpEntity(Request request) {
        return doHttpEntity(request, null);
    }

    private <R> HttpEntity doHttpEntity(Request request, R requestBody) {
        return new HttpEntity(requestBody, null);
    }

}