package com.gustavodamaral.clubsupporter.communication.config;

import java.util.Map;

public interface RestServiceConfiguration {

    String getScheme();
    String getHost();
    String getPort();

    Map<String, String> fetchHeader();

}
