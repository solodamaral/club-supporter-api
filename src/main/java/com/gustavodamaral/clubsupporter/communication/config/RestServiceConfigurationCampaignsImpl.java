package com.gustavodamaral.clubsupporter.communication.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class RestServiceConfigurationCampaignsImpl implements RestServiceConfiguration {

    @Value(value = "${api.clubsupporter.campaign.scheme:http}")
    private String scheme;
    @Value(value = "${api.clubsupporter.campaign.ip}")
    private String host;
    @Value(value = "${api.clubsupporter.campaign.port:}")
    private String port;

    @Override
    public String getSystemName() {
        return "Campaigns";
    }

}