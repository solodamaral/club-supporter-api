package com.gustavodamaral.clubsupporter.communication.url;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public interface RestTemplateStrategyBuilder {

    RestTemplate build() throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException;
    UrlBuilder getUrlBuilder();
}