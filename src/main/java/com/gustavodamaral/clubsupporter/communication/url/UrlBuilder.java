package com.gustavodamaral.clubsupporter.communication.url;

import com.gustavodamaral.clubsupporter.communication.config.RestServiceConfiguration;
import com.gustavodamaral.clubsupporter.communication.strategy.Request;
import lombok.Getter;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class UrlBuilder {

    @Getter
    private RestServiceConfiguration restServiceURI;

    public UrlBuilder(RestServiceConfiguration restServiceURI) {
        this.restServiceURI = restServiceURI;
    }

    public void fetchUrl(Request request) {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance().
                scheme(restServiceURI.getScheme()).
                host(restServiceURI.getHost());

        builder.path(request.getUrl());

        if (!restServiceURI.getPort().isEmpty())
            builder.port(restServiceURI.getPort());

        request.setUrl(builder.toUriString().replaceAll("%5B|%5D|%20", ""));
    }
}
