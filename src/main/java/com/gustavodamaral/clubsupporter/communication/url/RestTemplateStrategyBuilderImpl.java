package com.gustavodamaral.clubsupporter.communication.url;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

@Slf4j
@Component
@Getter
public class RestTemplateStrategyBuilderImpl implements RestTemplateStrategyBuilder {

    private UrlBuilder urlBuilder;

    private CloseableHttpClient httpClient;

    public RestTemplateStrategyBuilderImpl(UrlBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
        httpClient = HttpClients.custom().build();
    }

    public RestTemplate build() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }

}